<?php 
/* 
 * Configuração de Commit 
 * 
 * _commitPasta: Seta a pasta dos commits serão recebidos
 * _commitPrefixoArquivo: Seta o prefixo dos arquivos de commit
	 */
$commitPasta 			= 'commits';
$commitPrefixoArquivo 	= 'commit-';

/* 
 * Configuração de Deploy 
 * 
 * _deployAutomatico: Seta se o deploy será realizado automáticamente (Padrão é true - Sim)
 * _deployBranch: Seta a Branch que será utilizada
 */
$deployAutomatico 		= true;
$deployBranch 			= 'master';

/* 
 * Configuração de Configuração do Bitbucket 
 * 
 * _apiUsuario: Seta seu usuário do bitbucket
 * _apiSenha: Seta sua senha do bitbucket 
 */
$apiUsuario 			= '';
$apiSenha 				= '';

/* 
 * Configuração de Sistema 
 * 
 * _verbose: ...
 */
$verbose 				= true;

/* 
 * Configuração de Autenticação 
 * 
 * _requerAutenticacao: ...
 * _deployAutenticacao: ...
 * _gatewayAutenticacao: ...
 */
$requerAutenticacao 	= false;
$deployAutenticacao 	= '';
$gatewayAutenticacao 	= '';

/* 
 * Configuração de Deploys e Branchs
 * 
 * _deploys: Seta o(s) projeto(s) que irá(ão) ser utilizado(s) e sua(s) pasta(s) de destino no servidor
 *
 ** Multiplos Projetos - Exemplo **
 * 
 * $deploys = array(
 * 		'my-project-name' => '/home/www/site/', 
 * 		'my-project-name-2' => '/home/www/site-2/' 
 * );
 * 
 * OPCIONAL!
 * _branchs: Seta a(s) branch(s) que irá(ão) ser utilizada(s) e seu(s) projeto(s) de origem
 * 
 * 
 * $branchs = array(
 * 		'my-project-name' => 'master', 
 * 		'my-project-name-2' => 'development' 
 * );
 *
 * OR
 * 
 * $branchs = array(
 * 		'my-project-name' => array(
 * 			'dev' => '/var/www/public_html/desenvolvimento', 
 * 			'master' => '/var/www/public_html/'
 * 		)
 * );
 */
$deploys = array('my-project-name' => '/var/www/public_html/');