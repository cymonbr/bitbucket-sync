<?php 
/* Não permite acesso direto ao arquivo */
defined('_BSYNC') or die('Acesso negado!'); 

class system 
{
	private $_config;
	private $_deploy;

	private $_acao 		= '';
	private $_chave 	= '';

	private $_limpar 	= 0;
	private $_projeto 	= '';
	private $_repetir 	= '';

	private $_erro 		= false;
	private $_mensagem 	= '';

	public function __construct()
	{
		$this->_config = new config();

		$this->trataGets();
	}

	public function getAcao() { return $this->_acao; }
	public function getChave() { return $this->_chave; }
	public function getLimpar() { return $this->_limpar; }

	private function trataGets()
	{
		if(isset($_GET['acao'])&&(trim($_GET['acao'])=='deploy'||trim($_GET['acao'])=='gateway'))
		{
			$this->_acao = trim($_GET['acao']);
		}
		else
		{
			$this->_erro 		= true;
			$this->_mensagem 	.= (trim($this->_mensagem)!=''?'<br />':'').'Não foi possível localizar a ação';
		}

		if(isset($_GET['chave'])&&trim($_GET['chave'])!='')
		{
			$this->_chave = trim(strip_tags(stripslashes(urlencode($_GET['chave']))));
		}

		if(isset($_GET['limpar'])&&(int)$_GET['limpar']==1)
		{
			$this->_limpar = 1;
		}

		if(isset($_GET['projeto'])&&trim($_GET['projeto'])!='')
		{
			$this->_projeto = trim(strip_tags(stripslashes(urldecode($_GET['projeto']))));
		}
		else if(isset($_GET['repetir'])&&(int)$_GET['repetir']==1)
		{
			$this->_repetir = 1;
		}
		else if($this->_acao!='gateway'&&!isset($_GET['force']))
		{
			$this->_erro 		= true;
			$this->_mensagem 	.= (trim($this->_mensagem)!=''?'<br />':'').'Não foi possível localizar o repositório';
		}
	}

	private function httpResponseCode()
	{
		// For 4.3.0 <= PHP <= 5.4.0
		if (!function_exists('http_response_code'))
		{
		    function http_response_code($newcode = NULL)
		    {
		        static $code = 200;
		        if($newcode !== NULL)
		        {
		            header('X-PHP-Response-Code: '.$newcode, true, $newcode);
		            if(!headers_sent())
		                $code = $newcode;
		        }       
		        return $code;
		    }
		}
	}

	private function deploy()
	{
		$this->httpResponseCode();

		$this->_deploy = new deploy;
		$this->_deploy->setConfig($this->_config);
		$this->_deploy->setLimpar($this->_limpar);

		if($this->_projeto!='') 
		{
			/* Sincronização Completa */
			$this->_deploy->sincroniaCompleta($this->_chave, $this->_projeto);
		} 
		else if($this->_repetir) 
		{
			/* Repetir Sincronização que Falhou */
			$this->logInfo('<h3>Repetir Sincronização que Falhou</h3>');
			$this->_deploy->sincroniaAlteracoes($this->_chave, true);
		} 
		else 
		{
			/* Sincronizar Commit */
			$this->logInfo('<h3>Sincronizar Commits</h3>');
			$this->_deploy->sincroniaAlteracoes($this->_chave);
		}
	}

	private function gateway()
	{
		$this->httpResponseCode();

		$arquivo 	= $this->_config->getCommitPrefixoArquivo() . time() . '-' . rand(0, 100);
		$diretorio 	= $this->_config->getCommitPasta() . (substr($this->_config->getCommitPasta(), -1) == '/' ? '' : '/');

		if (!$this->_config->getRequerAutenticacao()||$this->_config->getRequerAutenticacao()&&$this->_config->getGatewayAutenticacao()==$this->_chave) 
		{
			if(!empty($_POST['payload'])) 
			{
				// store commit data
				if (get_magic_quotes_gpc()) 
				{
					file_put_contents($diretorio.$arquivo, stripslashes($_POST['payload']));
				} 
				else 
				{
					file_put_contents($diretorio.$arquivo, $_POST['payload']);
				}

				// process the commit data right away
				if($this->_config->getDeployAutomatico()) 
				{
					$this->_chave = $this->_config->getDeployAutenticacao();
					$this->deploy();
				}

			} 
			else if(isset($_GET['teste'])) 
			{
				if(file_put_contents($diretorio.'teste', 'Arquivos podem ser criados pelo script de gateway.')===false) 
				{
					echo "Este script não tem permissão para criar arquivos em ".$diretorio;
				} 
				else 
				{
					echo "Arquivos podem ser criados por este script em ".$diretorio;
				}
			}
			else 
			{
				echo 'Não foi possível localizar o POST vindo do Bitbucket, nenhuma ação a ser realizada!';
			}
		}
		else 
		{
			http_response_code(401);
		}
	}

	public function iniciar()
	{
		ob_start();

		if($this->_erro||$this->_config->getVerbose())
		{
			include(PATH.DS.'inc'.DS.'topo.phtml');
		}

		if(!$this->_erro)
		{
			if($this->_acao=='deploy')
			{
				$this->deploy();
			}
			else if($this->_acao=='gateway')
			{
				$this->gateway();
			}
		}
		else
		{
			echo $this->_mensagem;
		}

		if($this->_erro||$this->_config->getVerbose())
		{
			include(PATH.DS.'inc'.DS.'rodape.phtml');
		}
	}

	private function logInfo($message) 
	{
		if($this->_config->getVerbose()) 
		{
			echo $message;
			ob_end_flush();
	        @ob_flush();
	        flush();
	        ob_start();
		}
	}
}