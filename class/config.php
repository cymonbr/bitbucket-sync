<?php 
/*
 * BitBucket Sync (c) Cymon Designer
 * 
 * https://bitbucket.org/cymonkoz/bitbucket-sync
 * 
 * Arquivo: config.php
 * Versão: 1.0.0
 * Descrição: Arquivo de configuração do script Bitbucket Sunc.
 * 
 * Este programa é software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da GNU General Public License
 * como publicado pela Free Software Foundation; tanto a versão 2
 * da Licença, ou (a seu critério) qualquer versão posterior.
 * Este programa é distribuído na esperança que possa ser útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM DETERMINADO FIM. veja a
 * Licença Pública Geral GNU para obter mais detalhes.
 * 
 * 
 * BASEADO NO BITBUCKET SYNC ESCRITO POR Alex Lixandru
 * 
 */
 
/* Não permite acesso direto ao arquivo */
defined('_BSYNC') or die('Acesso negado!'); 

class config 
{
	/* 
	 * Variáveis de Commit 
	 */
	private $_commitPasta 			= '';
	private $_commitPrefixoArquivo 	= '';

	/* 
	 * Variáveis de Deploy 
	 */
	private $_deployAutomatico 		= '';
	private $_deployBranch 			= '';

	/* 
	 * Variáveis de Configuração do Bitbucket 
	 */
	private $_apiUsuario 			= '';
	private $_apiSenha 				= '';

	/* 
	 * Variável de Sistema 
	 */
	private $_verbose 				= '';

	/* 
	 * Variáveis de Autenticação 
	 */
	private $_requerAutenticacao 	= '';
	private $_deployAutenticacao 	= '';
	private $_gatewayAutenticacao 	= '';

	/* 
	 * Variável de Deploys e Branchs
	 */
	private $_deploys 				= array();
	private $_branchs 				= array();

	/* 
	 * Função de construção da classe 
	 * 
	 * Aqui são setados todos os itens que serão inicializados junto com a classe
	 */
	public function __construct()
	{
		$this->carregarConfiguracao();
	}

	public function getCommitPasta() { return $this->_commitPasta; }
	public function getCommitPrefixoArquivo() { return $this->_commitPrefixoArquivo; }

	public function getDeployAutomatico() { return $this->_deployAutomatico; }
	public function getDeployBranch() { return $this->_deployBranch; }

	public function getAPIUsuario() { return $this->_apiUsuario; }
	public function getAPISenha() { return $this->_apiSenha; }

	public function getVerbose() { return $this->_verbose; }

	public function getRequerAutenticacao() { return $this->_requerAutenticacao; }
	public function getDeployAutenticacao() { return $this->_deployAutenticacao; }
	public function getGatewayAutenticacao() { return $this->_gatewayAutenticacao; }

	public function getDeploys() { return $this->_deploys; }
	public function getBranchs() { return $this->_branchs; }

	private function carregarConfiguracao()
	{
		include(PATH.DS.'configuracao.php');

		/* 
		 * Configuração de Commit 
		 * 
		 * _commitPasta: Seta a pasta dos commits serão recebidos
		 * _commitPrefixoArquivo: Seta o prefixo dos arquivos de commit
	 	 */
		$this->_commitPasta 			= $commitPasta;
		$this->_commitPrefixoArquivo 	= $commitPrefixoArquivo;

		/* 
		 * Configuração de Deploy 
		 * 
		 * _deployAutomatico: Seta se o deploy será realizado automáticamente (Padrão é true - Sim)
		 * _deployBranch: Seta a Branch que será utilizada
		 */
		$this->_deployAutomatico 		= $deployAutomatico;
		$this->_deployBranch 			= $deployBranch;

		/* 
		 * Configuração de Configuração do Bitbucket 
		 * 
		 * _apiUsuario: Seta seu usuário do bitbucket
		 * _apiSenha: Seta sua senha do bitbucket 
		 */
		$this->_apiUsuario 				= $apiUsuario;
		$this->_apiSenha 				= $apiSenha;

		/* 
		 * Configuração de Sistema 
		 * 
		 * _verbose: ...
		 */
		$this->_verbose 				= $verbose;

		/* 
		 * Configuração de Autenticação 
		 * 
		 * _requerAutenticacao: ...
		 * _deployAutenticacao: ...
		 * _gatewayAutenticacao: ...
		 */
		$this->_requerAutenticacao 		= $requerAutenticacao;
		$this->_deployAutenticacao 		= $deployAutenticacao;
		$this->_gatewayAutenticacao 	= $gatewayAutenticacao;

		/* 
		 * Configuração de Deploys e Branchs
		 * 
		 * _deploys: Seta o(s) projeto(s) que irá(ão) ser utilizado(s) e sua(s) pasta(s) de destino no servidor
		 *
		 ** Multiplos Projetos - Exemplo **
		 * 
		 * $this->_deploys = array(
		 * 		'my-project-name' => '/home/www/site/', 
		 * 		'my-project-name-2' => '/home/www/site-2/' 
		 * );
		 * 
		 * OPCIONAL!
		 * _branchs: Seta a(s) branch(s) que irá(ão) ser utilizada(s) e seu(s) projeto(s) de origem
		 * 
		 * 
		 * $this->_branchs = array(
		 * 		'my-project-name' => 'master', 
		 * 		'my-project-name-2' => 'development' 
		 * );
		 */
		$this->_branchs = $branchs;
		$this->_deploys = $deploys;
	}
}