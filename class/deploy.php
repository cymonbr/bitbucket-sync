<?php 
/* Não permite acesso direto ao arquivo */
defined('_BSYNC') or die('Acesso negado!'); 

class deploy 
{
	private $_config;
	private $_limpar 	= 0;

	private $_processed = 0;
	private $_removeDir = 0;

	public function setConfig($value) { $this->_config = $value; }
	public function setLimpar($value) { $this->_limpar = $value; }

	/*
	 * Obtém o conteúdo completo do repositório e armazena localmente
	 */
	public function sincroniaCompleta($chave, $repositorio) 
	{
		$deploys = $this->_config->getDeploys();
		$branchs = $this->_config->getBranchs();

		/* Checa a chave de autenticação se for requerida */
		if($this->_limpar&&$this->_config->getDeployAutenticacao()=='') 
		{
			/* Quando chama a limpeza, a chave de autenticação é obrigatória, independentemente da bandeira 'requireAuthentication' */
			http_response_code(403);
			$this->logInfo(" # Não é possível realizar a limpeza agora. É necessário definir uma chave de autorização para realizar a limpeza.\n");
			return false;
		} 
		else if(($this->_config->getRequerAutenticacao()||$this->_limpar)&&$this->_config->getDeployAutenticacao()!=$chave) 
		{
			http_response_code(401);
			$this->logInfo("  # Não autorizado. ".($this->_limpar && empty($chave)?" A chave de autenticação de deploy deve ser fornecida para realizar a limpeza.":""));
			return false;
		}

		$this->logInfo("<pre>\n<h3>BitBucket Sync - Sincronização Completa</h3>============================\n");

		/* Determina o destino dos arquivos do deploy */
		if(array_key_exists($repositorio, $deploys)) 
		{
			$deployDestino = $deploys[$repositorio].(substr($deploys[$repositorio], -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		} 
		else 
		{
			$this->logInfo(" # Repositório desconhecido: <i>'".$repositorio."'</i>\n");
			return false;
		}

		/* Determinar a partir de qual Branch deve fazer o download */
		if(array_key_exists($repositorio, $branchs)) 
		{
			$deployBranch = $branchs[$repositorio];
		} 
		else 
		{
			/* Usa a Branch padrão */
			$deployBranch = $this->_config->getDeployBranch();
		}

		if(is_array($deployBranch))
		{
			$this->logInfo(" <h4>Multi Branch</h4>");
			foreach($deployBranch as $branch => $caminho)
			{
				$this->logInfo(" <h5>* Copiando a Branch <i>'".$branch."'</i></h5>");
				$this->downloadDeploy($repositorio, $branch, $caminho);
			}
		}
		else
		{
			$this->logInfo(" <h5>* Copiando a Branch <i>'".$deployBranch."'</i></h5>");
			$this->downloadDeploy($repositorio, $deployBranch, $deployDestino);
		}

		$this->logInfo("\n============================<h4>Deploy do repositório <i>'".$repositorio."'</i> concluído.</h4></pre>");
	}

	/*
	 * Sincroniza as alterações dos arquivos de commit
	 */
	public function sincroniaAlteracoes($chave, $repetir=false) 
	{
		/* Checa a chave de autenticação se for requerida */
		if ($this->_config->getRequerAutenticacao() && $this->_config->getDeployAutenticacao()!=$chave) 
		{
			http_response_code(401);
			echo " # Unauthorized";
			return false;
		}

		$this->logInfo("<pre>\n<h3>BitBucket Sync - Sincronização Alterações</h3>============================\n\n");

		$prefixo = $this->_config->getCommitPrefixoArquivo();
		if($repetir) 
		{
			$prefixo = "failed-".$prefixo;
		}

		$this->_processed 	= array();
		$this->_removeDir 	= array();
		$diretorio 			= $this->_config->getCommitPasta().(substr($this->_config->getCommitPasta(), -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		$commits 			= scandir($diretorio, 0);

		if($commits)
		{
			$this->logInfo(" * Listando os arquivos de commit\n");
			foreach($commits as $arquivo) 
			{
				if($arquivo!='.'&&$arquivo!='..'&&$arquivo!='leia-me.txt'&&is_file($diretorio.$arquivo)&&stripos($arquivo, $prefixo)===0) 
				{
					/* Obtem o conteúdo do commit e faz a analise */
					$json 	= file_get_contents($diretorio.$arquivo);

					$del 	= true;
					$this->logInfo(" <h4>Processando o arquivo <i>'".$arquivo."'</i></h4>");
					if(!$json||!$this->deployAlteracoes($json)) 
					{
						$this->logInfo(" * Não foi possível processar o arquivo <i>'".$arquivo."'</i>\n");
						$del = false;
					}

					if($del) 
					{
						/* Exclui o arquivo após o processo */
						//unlink($diretorio.$arquivo);
					} 
					else 
					{
						/* Mantem o arquivo se falhar para processamento posterior */
						if(!$repetir) rename($diretorio.$arquivo, $diretorio.'failed-'.$arquivo);
					}
				}
			}
		}

		/* Remover os diretórios antigos (renomeados) que estão vazios */
		foreach($this->_removeDir as $dir => $nome) 
		{
			if(rmdir($dir)) 
			{
				$this->logInfo("\n * Diretório removido: <i>'".$nome."'</i>\n");
			}
		}

		$this->logInfo("\n============================<h4>Concluído processo de commit.</h4></pre>");
	}

	/*
	 * Deploy commits com o sistema de arquivos
	 */
	public function deployAlteracoes($postData) 
	{
		$deploys 	= $this->_config->getDeploys();
		$branchs 	= $this->_config->getBranchs();

		$dadosJson 	= json_decode($postData);

		if(!$dadosJson) 
		{
			/* Não foi possível localizar os dados */
			$this->logInfo("|    ! Arquivo JSON inválido!\n");
			return false;
		}

		/* Determinar o destino do deploy */
		if(array_key_exists($dadosJson->repository->slug, $deploys)) 
		{
			$deployDestino = $deploys[$dadosJson->repository->slug].(substr($deploys[$dadosJson->repository->slug], -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		} 
		else 
		{
			/* Repositório desconhecido */
			$this->logInfo("|    ! Repositório não configurado para sincronia: <i>'".$dadosJson->repository->slug."'</i>!\n");
			return false;
		}

		/* Determina a partir de qual Branch serão obtido os dados */
		if(isset($branchs) && array_key_exists($dadosJson->repository->slug, $branchs)) 
		{
			$deployBranch = $branchs[$dadosJson->repository->slug];
		} 
		else 
		{
			/* Utiliza o Branch padrão */
			$deployBranch = $this->_config->deployBranch();
		}

		/* Cria a URL para obter os arquivos a serem atualizados */
		$commitUrl 	= $dadosJson->canon_url; 					# https://bitbucket.org
		$commitUrl 	.= '/api/1.0/repositories'; 				# /api/1.0/repositories
		$commitUrl 	.= $dadosJson->repository->absolute_url; 	# /user/repo/
		$commitUrl 	.= 'raw/'; 									# raw/

		if(is_array($deployBranch))
		{
			foreach($deployBranch as $branch => $caminho)
			{
				$this->processaCommit($commitUrl, $branch, $caminho, $dadosJson);
			}
		}
		else
		{
			$this->processaCommit($commitUrl, $deployBranch, $deployDestino, $dadosJson);
		}

		return true;
	}

	/*
	 * Obtém um conteúdo de arquivos remotos usando CURL
	 */
	private function obterConteudo($url, $gravarArquivo=false) 
	{
		/* Cria um novo recurso cURL */
		$ch = curl_init();

		/* Seta URL e outras opções */
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);

		if ($gravarArquivo) 
		{
			$saida = fopen($gravarArquivo, "wb");
			if ($saida==FALSE) 
			{
				throw new Exception("Não foi possível abrir o arquivo '".$gravarArquivo."' em modo escrita");
			}
			curl_setopt($ch, CURLOPT_FILE, $saida);
		} 
		else 
		{
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		}

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->_config->getAPIUsuario().':'.$this->_config->getAPISenha());
		/* Descomente o item abaixo para escolher a versão do SSL que deseja */
		//curl_setopt($ch, CURLOPT_SSLVERSION,3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

		/* Executa a URL */
		$dados = curl_exec($ch);

		/* Verifica se há erro e se houver exibe */
		if(curl_errno($ch) != 0) 
		{
			$this->logInfo("      ! Erro na tranferência de arquivo: ".curl_error($ch)."\n");
		}

		/* Finaliza o cURL para liberar recursos para o sistema */
		curl_close($ch);

		return $dados;
	}

	/*
	 * Executa download do arquivo do deploy
	 */
	private function downloadDeploy($repositorio, $deployBranch, $deployDestino)
	{
		/* Constrói URL para download do arquivo */
		$baseUrl 		= 'https://bitbucket.org/';
		$repoUrl 		= (!empty($_GET['team'])?$_GET['team']:$this->_config->getAPIUsuario())."/".$repositorio."/";
		$branchUrl 		= 'get/'.$deployBranch.'.zip';

		/* Armazenar arquivo ZIP temporário */
		$zipArquivo 	= 'full-'.time().'-'.rand(0, 100);
		$zipDestino 	= $this->_config->getCommitPasta().(substr($this->_config->getCommitPasta(), -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);

		/* Download do arquivo */
		$this->logInfo(" * Fazendo download do arquivo <i>'".$baseUrl.$repoUrl.$branchUrl."'</i>\n");
		$result 		= $this->obterConteudo($baseUrl.$repoUrl.$branchUrl, $zipDestino.$zipArquivo);

		/* Extrair conteúdo */
		$this->logInfo(" * Extraindo o arquivo para <i>'".$zipDestino."'</i>\n");
		$zip = new ZipArchive;
		if($zip->open($zipDestino.$zipArquivo)===true) 
		{
			$zip->extractTo($zipDestino);
			$stat 	= $zip->statIndex(0); 
			$pasta 	= $stat['name'];
			$zip->close();
		} 
		else 
		{
			$this->logInfo(" # Não foi possível extrair os arquivos. O nome do repositório correto?\n");
			unlink($zipDestino.$zipArquivo);
			return false;
		}

		/* Validar conteúdo extraído */
		if(empty($pasta)||!is_dir($zipDestino.$pasta)) 
		{
			$this->logInfo(" # Não foi possível localizar os arquivos extraídos em <i>'".$zipDestino."'</i>\n");
			unlink($zipDestino.$zipArquivo);
			return false;
		}

		/* Excluir os arquivos antigos, se for instruído a fazê-lo */
		if($this->_limpar) 
		{
			$this->logInfo(" * Excluindo os arquivos antigos de <i>'".$deployDestino."'</i>\n");
			if($this->deletarPastas($deployDestino)===false) 
			{
				$this->logInfo(" # Não foi possível remover completamente os arquivos antigos de <i>'".$deployDestino."'</i>. Processo continuará de qualquer maneira!\n");
			}
		}

		/* Copiar conteúdo para pasta de produção */
		$this->logInfo(" * Copiando o novo conteúdo para <i>'".$deployDestino."'</i>\n");
		if($this->copiarPastas($zipDestino.$pasta, $deployDestino)==false) 
		{
			$this->logInfo(" # Não foi possível extrair os arquivos do deploy para <i>'".$deployDestino."'</i>. Deploy está incompleto!\n");
			$this->deletarPastas($zipDestino.$pasta, true);
			unlink($zipDestino.$zipArquivo);
			return false;
		}

		/* Limpar */
		$this->logInfo(" * Limpar arquivos e pastas temporárias\n");
		$this->deletarPastas($zipDestino.$pasta, true);
		unlink($zipDestino.$zipArquivo);
	}

	/*
	 * Processa o arquivo de commit
	 */
	private function processaCommit($commitUrl, $deployBranch, $deployDestino, $dadosJson)
	{
		/* Insere a branch na URL que busca os arquivos do commit */
		$commitUrl 	.= $deployBranch.'/'; # branch/

		/* Prepara para processar os arquivos */
		$pendente 	= array();

		/* Loop dos commits */
		foreach($dadosJson->commits as $commit) 
		{
			/* Checa se o Branch existe nesta etapa */
			if(!empty($commit->branch)||!empty($commit->branches)) 
			{
				/* Se o commit for no branch setado, efetua as alterações */
				if($commit->branch==$deployBranch||(!empty($commit->branches)&&array_search($deployBranch, $commit->branches)!==false)) 
				{
					$this->logInfo(" <h5>* Processando commit da Branch <i>'".$deployBranch."'</i></h5>");
					$this->logInfo("<h4>|    > Change-set: ".trim($commit->message)."</h4>");

					/* Se existirem arquivos pendentes, junta aos arquivos a serem processados */
					$arquivos = array_merge($pendente, $commit->files);

					/* Loop da lista de arquivos */
					foreach($arquivos as $arquivo) 
					{
						if($arquivo->type=='modified'||$arquivo->type=='added') 
						{
							if(empty($this->_processed[$arquivo->file])) 
							{
								$this->_processed[$arquivo->file] = 1; /* Marca o arquivo como processado */
								$conteudo = $this->obterConteudo($commitUrl.$arquivo->file);
								if($conteudo=='Not Found') 
								{
									/* Realiza uma nova tentativa no caso do BitBucket está instável */
									$conteudo = $this->obterConteudo($commitUrl.$arquivo->file);
								}

								if($conteudo!='Not Found'&&$conteudo!==false) 
								{
									if(!is_dir(dirname($deployDestino.$arquivo->file))) 
									{
										/* Tenta criar a estrutura de pasta */
										mkdir(dirname($deployDestino.$arquivo->file), 0755, true);
									}

									file_put_contents($deployDestino.$arquivo->file, $conteudo);
									$this->logInfo("|      - ".($arquivo->type=='added'?'Adicionado':'Modificado').": <i>'".$arquivo->file."'</i>\n");
								} 
								else 
								{
									$this->logInfo("|      - Não foi possível obter o conteúdo do arquivo <i>'".$arquivo->file."'</i>\n");
								}
							}

						} 
						else if($arquivo->type=='removed') 
						{
							unlink($deployDestino.$arquivo->file);
							$this->_processed[$arquivo->file] = 0; /* Para permitir a re-criação posterior do arquivo */
							$this->_removeDir[dirname($deployDestino.$arquivo->file)] = dirname($arquivo->file);
							$this->logInfo("|      - Removido: <i>'".$arquivo->file."'</i>\n");
						}
					}
				}
				else
				{
					$this->logInfo("      - Branch não suportada para commit\n");
				}

				/* Limpar arquivos pendentes se existir */
				$pendente = array();
			} 
			else 
			{
				/* Branch desconhecido, por enquanto, manter os arquivos */
				$pendente = array_merge($pendente, $commit->files);
			}
		}
	}

	/*
	 * Copia o conteúdo do diretório, de forma recursiva, para o local especificado
	 */
	private function copiarPastas($diretorio, $destino) 
	{
		if (!file_exists($destino)) if(!mkdir($destino, 0755, true)) return false;
		if (!is_dir($diretorio)||is_link($diretorio)) return copy($diretorio, $destino); // should not happen

		$arquivos 	= array_diff(scandir($diretorio), array('.','..'));
		$sep 		= (substr($diretorio, -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		$dsp 		= (substr($destino, -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);

		foreach ($arquivos as $arquivo) 
		{
			(is_dir($diretorio.$sep.$arquivo))?$this->copiarPastas($diretorio.$sep.$arquivo, $destino.$dsp.$arquivo):copy($diretorio.$sep.$arquivo, $destino.$dsp.$arquivo);
		}

		return true;
	}

	/*
	 * Exclui um diretório de forma recursiva, não importa se ele está vazio ou não
	 */
	private function deletarPastas($diretorio, $deletarRaiz=false) 
	{
		if (!file_exists($diretorio)) return false;
		if (!is_dir($diretorio)||is_link($diretorio)) return unlink($diretorio);

		// prevent deletion of current directory
		$cdir = realpath($diretorio);
		$adir = PATH;
		$cdir = $cdir.(substr($cdir, -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		$adir = $adir.(substr($adir, -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);

		if($cdir==$adir) 
		{
			logInfo(" * Contents of '".basename($adir)."' folder will not be cleaned up.\n");
			return true;
		}

		// process contents of this dir
		$arquivos 	= array_diff(scandir($diretorio), array('.','..'));
		$separador 	= (substr($diretorio, -1)==DIRECTORY_SEPARATOR?'':DIRECTORY_SEPARATOR);
		foreach ($arquivos as $arquivo) 
		{
			(is_dir($diretorio.$separador.$arquivo))?$this->deletarPastas($diretorio.$separador.$arquivo, true):unlink($diretorio.$separador.$arquivo);
		}

		if($deletarRaiz) 
		{
			return rmdir($diretorio);
		} 
		else 
		{
			return true;
		}
	}

	/* 
	 * Executa flush de liberação do PHP se estiver setado o modo Verbose
	 */
	private function logInfo($message) 
	{
		if($this->_config->getVerbose()) 
		{
			echo $message;
			ob_end_flush();
	        @ob_flush();
	        flush();
	        ob_start();
		}
	}
}