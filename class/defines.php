<?php 
/* Não permite acesso direto ao arquivo */
defined('_BSYNC') or die('Acesso negado!'); 

/* Definindo as pastas do sistema */
define( 'CLASSES', PATH.DS.'class'.DS );

/* Chamada dos arquivos de Classe */
require_once(CLASSES.'config.php');
require_once(CLASSES.'deploy.php');
require_once(CLASSES.'system.php');

$app = new system();
$app->iniciar();