<?php 
/* Força a exibição de erros do PHP */
/*ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);*/

ini_set('memory_limit', '-1');
$old = ini_get('default_socket_timeout');
ini_set('default_socket_timeout', 3000);
set_time_limit(0);
ini_set('max_execution_time', 3600);

define('_BSYNC', 1);
define('DS', DIRECTORY_SEPARATOR);
define('PATH', dirname(__FILE__));

/* 
 * Chama a classe de definições do sistema
 * 
 * A classe faz o tratamento das classes e inicia o processo solicitado
 */
require_once(PATH.DS.'class'.DS.'defines.php');